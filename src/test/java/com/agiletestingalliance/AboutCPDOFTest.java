package com.agiletestingalliance;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import static org.junit.Assert.*;

public class AboutCPDOFTest {



   @Test
    public void testDesc() throws Exception {
        String desc = new AboutCPDOF().desc();
        assertEquals("Add", true, desc.contains("CP-DOF certification program covers end to end DevOps Life Cycle"));
    }



}
